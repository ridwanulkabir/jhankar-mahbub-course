import React from 'react';

const Userfriend = ({user}) => {
    const {name,email} =user
    return (
        <div className='box'>
            <h3>Name: {name}</h3>
            <h3>Email: {email}</h3>
        </div>
    );
};

export default Userfriend;