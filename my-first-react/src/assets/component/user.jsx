import React, { useEffect, useState } from 'react';
import './user.css'
import Userfriend from './Userfriend';

const User = () => {
    const [user, setuser] = useState([])
    useEffect(()=>{
        fetch('https://jsonplaceholder.typicode.com/users')
        .then(res=>res.json())
        .then(data=> setuser(data))
    
    },[])
    return (
        <div className='box'>
            <h2>User {user.length}</h2>
        { user.map(user=> <Userfriend user={user}></Userfriend>)     }
        </div>
    );
};

export default User;