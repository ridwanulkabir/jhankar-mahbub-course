function loadpost(){
 fetch('https://jsonplaceholder.typicode.com/posts')
 .then(res => res.json())
 .then(data => displaypost(data))
}
function displaypost(post){
    const postContainer= document.getElementById('post-container')

    for(const display of post) {
        const div = document.createElement('div')
        console.log(display)
        div.innerHTML =` 
        <h4> user-${display.userId}<h4>
        <h5> post:${display.title} <h5>
        <p> post description: ${display.body} <p>
        
        `
        postContainer.appendChild(div)
    }

}
loadpost()