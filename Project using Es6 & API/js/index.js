// step 1 : API convert data to json
const handlecategory = async () => {
   const response= await fetch('https://openapi.programming-hero.com/api/news/categories')
   const data = await response.json()

   
   // step 2 : get to id for dynamic data show
   const tabcontainer = document.getElementById("tab-container")
   // step 3 : data foreach, new div creat, appenchild, slice data....(remember~ f=loop always work for array not object)
   data.data.news_category.slice(0,3).forEach(category => { 
      const div = document.createElement("div")
      div.innerHTML =`
      <a onclick="handleloadnews('${category.category_id}')" class="tab tab-bordered">${category.category_name}</a> 

      `
      tabcontainer.appendChild(div)
   });
}
const handleloadnews= async(categoryid)=>{
   const response= await fetch(`https://openapi.programming-hero.com/api/news/category/${categoryid}`)
   const data = await response.json()
   
   const cardcontainer = document.getElementById("card-container")
   data.data.forEach(news => {
   console.log(news)

    const div = document.createElement("div")
    div.innerHTML = `   <div class="card w-96 bg-base-100 shadow-xl">
    <figure class="px-10 pt-10">
      <img src="${news.image_url}" alt="Shoes" class="rounded-xl" />
    </figure>
    <div class="card-body items-center text-center">
      <h2 class="card-title">${news.title.slice(0,50)}</h2>
      <p>${news.details.slice(0,120)}</p>
      <div class="card-actions">
        <button class="btn btn-primary">Details</button>
      </div>
    </div>
  </div> 
    `
    cardcontainer.appendChild(div)
   })
   
}
handlecategory()