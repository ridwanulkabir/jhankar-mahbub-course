// multiline string using bscktick(`)
const message = `Ridwan 
basa ctg`
console.log(message)

// for dynamic string we need to use placeholder ${} 
const a= 10 
const b = 12 
const message = `my result of ${a} and ${b} is ${a+b}`
console.log(message)