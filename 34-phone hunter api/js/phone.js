function loaddata(searchText){
    fetch(` https://openapi.programming-hero.com/api/phones?search=${searchText}`)
    .then(res => res.json())
    .then(data => {displayphone(data.data)})

}

const displayphone= phones =>{
    const phoneContainer = document.getElementById('phone-container')
    // clear the phone container before adding new cards 
    phoneContainer.textContent= ''
phones.forEach(element => {
console.log(element)
// creat a div 
const phoneCard = document.createElement('div')
phoneCard.innerHTML = `<div class="card card-compact bg-base-100 p-2 shadow-xl">
<figure><img src="${element.image}" alt="Shoes" /></figure>
<div class="card-body">
  <h2 class="card-title">${element.phone_name}</h2>
  <p>If a dog chews shoes whose shoes does he choose?</p>
  <div class="card-actions justify-end">
    <button class="btn btn-primary">Buy Now</button>
  </div>
</div>
</div>

`
// append child 
phoneContainer.appendChild(phoneCard)
})
}
// handle search button 
const handlesearch =()=> {
  const searchField =document.getElementById('searchField')
  const searchText = searchField.value
  loaddata(searchText)
}

